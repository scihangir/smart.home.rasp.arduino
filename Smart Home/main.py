#!/usr/bin/python
from firebase import firebase
import time
import os
import dht11
import time
import threading
import RPi.GPIO as GPIO



GPIO.setmode(GPIO.BCM)

# init list with pin numbers        
pinList = [2, 3, 26, 17, 27, 23, 24]

# loop through pins and set mode and state to 'high'

for i in pinList:
    GPIO.setup(i, GPIO.OUT)
    GPIO.output(i, GPIO.HIGH)

# Firebase hesabina baglaniyoruz ...
firebase = firebase.FirebaseApplication('https://smarthome-308a5.firebaseio.com')


def readTempHum():
    instance = dht11.DHT11(pin=21)
    result = instance.read()

    # add current value
    firebase.put("/Controls/Sensors", "/Humidity/current_inside", "" + str(result.humidity) + "%")
    firebase.put("/Controls/Sensors", "/Temperature/current_inside", "" + str(result.temperature) + "C")

    ##retrive max & min humidity (remove the %)
    maxHumidity = firebase.get("/Controls/Sensors/Humidity/max_inside", None)
    maxHumidity = maxHumidity[:-1]

    minHumidity = firebase.get("/Controls/Sensors/Humidity/min_inside", None)
    minHumidity = minHumidity[:-1]

    # retrieve max & min temperature (remove the C)
    maxTemperature = firebase.get("/Controls/Sensors/Temperature/max_inside", None)
    maxTemperature = maxTemperature[:-1]

    minTemperature = firebase.get("/Controls/Sensors/Temperature/min_inside", None)
    minTemperature = minTemperature[:-1]

    ##check for max values
    if float(result.humidity) > float(maxHumidity):
        firebase.put("/Controls/Sensors", "/Humidity/max_inside", "" + str(result.humidity) + "%")
        print 'Updated Humidity max_inside'
    if float(result.temperature) > float(maxTemperature):
        firebase.put("/Controls/Sensors", "/Temperature/max_inside", "" + str(result.temperature) + "C")
        print 'Updated Temperature max_inside'

    ## check for min values
    if float(result.humidity) < float(minHumidity):
        firebase.put("/Controls/Sensors", "/Humidity/min_inside", "" + str(result.humidity) + "%")
        print 'Updated Humidity min_inside'
    if float(result.temperature) < float(minTemperature):
        firebase.put("/Controls/Sensors", "/Temperature/min_inside", "" + str(result.temperature) + "C")
        print 'Updated Temperature min_inside'


    time.sleep(30)



#*********************************************************************
#                            MAIN
#*********************************************************************
print("Executing program:")
t1  = threading.Thread(target = readTempHum)
t1.start()

while True:
        mutfakLED = firebase.get('/Lights/ODA1/', 'status')
        oturmaOdaLED = firebase.get('/Lights/ODA2/', 'status')
        yatakOdaLED=firebase.get('/Lights/ODA3/', 'status')
        sicaklik=firebase.get("/Controls/Sensors/Temperature/current_inside", None)
        sicaklik= sicaklik[:-1]

        yazModu=True
        kisModu=True

        if t1.is_alive()is False:
            del t1
            t1  = threading.Thread(target =readTempHum)
            t1.start()
        if oturmaOdaLED == 'True':
            GPIO.output(2, GPIO.LOW)
        if oturmaOdaLED == 'False':
            GPIO.output(2, GPIO.HIGH)
        if mutfakLED == 'True':
            GPIO.output(3, GPIO.LOW)
        if mutfakLED == 'False':
            GPIO.output(3, GPIO.HIGH)
        if yatakOdaLED == 'True':
            GPIO.output(26, GPIO.LOW)
        if yatakOdaLED == 'False':
            GPIO.output(26, GPIO.HIGH)
        if yazModu is True:
            if sicaklik>str(30):
                GPIO.output(17, GPIO.LOW)
            else:
                GPIO.output(17, GPIO.HIGH)
        if kisModu is True:
            if sicaklik<str(25):
                GPIO.output(27, GPIO.LOW)
            else:
                GPIO.output(27, GPIO.HIGH)
        time.sleep(1)



















